# Bhagaskara Bootstrap 3 Template #
Clean, responsive template based on Bhagaskara Onepage PSD Template.

* [PSD files](https://www.behance.net/gallery/16872707/Bhagaskara-Onepage-PSD-Template)
* [Live preview](http://dombas-interactive.com/works/bhagaskara/)

### What was my duties? ###

* Cutting PSD file
* Create HTML doc from the scratch
* Include [Twitter Bootstrap 3](http://getbootstrap.com/), [Slick](http://kenwheeler.github.io/slick/) and other libraries contained in 'lib' folder
* CSS classes & styles, @font-face, etc.
* Responsive Web Design
* jQuery scripts (lib/scripts.js)

### Technologies ###

* HTML 5
* CSS 3
* SASS
* jQuery