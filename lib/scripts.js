/*
 Bhagaskara jQuery script file
 Version: 1.0
 Author: Łukasz Dąbek
 Website: http://dombas-interactive.com/
 */

function fullHeight(){
    /*
     HOW IT WORKS:
     If content of each element is lower than window height, function sets full window height for this element.
     <div class="height-full"> <--- main element
     <div class="height-full-content"> <--- measured element
     </div>
     </div>
     */
    $('.height-full').each(function(){
        var contentHeight = $(this).find('.height-full-content').height();
        var windowHeight = $('body').height();
        if(windowHeight>=contentHeight) $(this).css('height','100vh');
        else $(this).css('height','');
    });
}
function navbarPosition() {
    var homeHeight = $('div.header').height();
    if($(this).scrollTop()>homeHeight) {
        $('.navbar-default').addClass('navbar-fixed-top').removeClass('navbar-start').css('top','');
        $('#features').css('margin-top','');
    }
    else {
        $('.navbar-default').removeClass('navbar-fixed-top').addClass('navbar-start').css('top',homeHeight);
        $('#features').css('margin-top','140px');
    }
}
function scrollToSection() {
    // Scrolling to specific section 'id' using href="#idName"
    $('.scroll-it').click(function(event){
        event.preventDefault();
        $('html,body').animate({
            scrollTop:$(this.hash).offset().top
        },1000);
    });
}

$('body').imagesLoaded(function() {
    setTimeout(function() {
        $('body').removeClass('loading').addClass('loaded');
    }, 800);
});
$(document).ready(function(){
    fullHeight();
    $('.team-slider-content').slick({
        autoplay: true,
        autoplaySpeed: 8000,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.testimonial-slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
        dots: true
    });
});
$(window).load(function() {
    $('.navbar-default').css('top',$('div.header').height());
    scrollToSection();

    $('.portfolio-inner a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $(window).resize(function() {
        fullHeight();
        navbarPosition();
    });

    $(window).scroll(function() {
        navbarPosition();
    });
});

